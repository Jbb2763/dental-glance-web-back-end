const functions = require('firebase-functions');
// The Firebase Admin SDK to access the Firebase Realtime Database.
const admin = require('firebase-admin');
const _ = require('lodash');

admin.initializeApp(functions.config().firebase);

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

// Practice Registration listener
// Assigns the authenticated user that creates a new Practice an 'admin' role
// Also creates an office by the same name. (For v1 we are associating one Office to each Practice automatically)

// Office creation listener
// Populates the office with associated # of operatories and default statuses

//  Events listener
// Listens for an Event record addition and triggers a push notification
exports.onOperatoryEventAdded = functions.database.ref('/Events/{eventId}').onWrite(e => {
    // Only edit data when it is first created.
    if (e.data.previous.exists()) {
        return;
    }
    const newEvent = e.data.val();
    // return e.data.ref.parent.child('testroute').set(newEvent);
    console.log(e.data.ref.root.child('DeviceRegistrationTokensByOffice/'+newEvent.officeId));
    console.log('newEvent: ', newEvent);
    let contentAvailable, payload;
    if (newEvent.status === 'ready') {
        // Standard APN
        contentAvailable = false;
        payload = {
            notification: {
                title: 'Dental Glance',
                body: `${newEvent.operatoryId} - Patient Ready`,
                sound: 'default'
            }
        };
    } else {
        // Silent APN
        contentAvailable = true;
        payload = {
            data: _.each(newEvent, (v,k) => {
                return newEvent[k] = String(v);}
            )
        };
    }
    e.data.ref.root.child('DeviceRegistrationTokensByOffice/'+newEvent.officeId).once('value', snapshot => {
        console.log('in database response', snapshot.val());
        let tokens = [];
        snapshot.forEach(childSnapshot => {
            tokens.push(childSnapshot.val());
        });
        console.log('tokens: ', tokens);
        // Send push notification
        /* Example
        const token = 'f-KD4PzzqNY:APA91bGxP8Lvh2C6l_ROHn-YK1bYKxoCWlx9gSaYh_Tngsu3OYlFkiPQ92JIeIejEKU2C-aTTaWgspBomfXxl-6DUN4IYBbPmdTN87mLvUomBZcOatdMy936WKmtlpnSZLAwbDFLSS4b';
        */
        admin.messaging().sendToDevice(tokens, payload, {contentAvailable: contentAvailable, priority: 'high'})
            .then(function(response) {
                // See the MessagingDevicesResponse reference documentation for
                // the contents of response.
                console.log("Push notification Logs:", (response.results[0]));
            });
    })
});

// Push Notification Sender
// Receives a payload and submits a silent push notification to users of a given Office


//Registrations listener (for admins only)
/*
1. Assigns a practice level permission with admin
2. Assigns an office permission
 */
exports.onRegistrationAdded = functions.database.ref('/Registrations/{practiceId}').onWrite(e => {
    const newRegistration = e.data.val();
    const uid = newRegistration.userId;
    const practiceId = e.params.practiceId;

    // Assign practice-level permission
    admin.database().ref('Permissions').child(`practice_${uid}`).set({
        permissionLevel: 'admin',
        practiceId: practiceId,
        userId: uid
    }).then(permission => {
        console.log('successfully saved Practice permission', permission);
    }, error => {
        console.log('error saving Practice permission', error);
    });

    // Assign office level permission
    //TODO: We shouldn't be generating offices on the fly, so remove this after testing.
    const officeId = admin.database().ref('/Offices').push().key;
    admin.database().ref('/Offices').child(officeId).set({
       name: 'Sample Office',
        street1: '123 Main St',
        city: 'Chicago',
        state: 'IL',
        zip: '60007',
        phone: '555-555-5555',
        practiceId: practiceId
    });
    admin.database().ref('/Permissions').child(`office_${uid}_${officeId}`).set({
        officeId: officeId,
        userId: uid
    }).then(permission => {
        console.log('successfully saved Office permission', permission);
    }, error => {
        console.log('error saving Office permission', error);
    });
});
