import React, { Component } from 'react';
import Footer from './Footer';
import Operatory from './Operatory';
import _ from 'lodash';
import moment from 'moment';
import {firebaseDatabaseService} from './firebase.database.fact'
import {Icon} from 'react-fa'

class Operatories extends Component {
    constructor(props) {
        super(props);
        this.state = {
            operatories: [],
            procedureCodes: [],
            office: {},
            refs: []
        };
        this.interval = {};
    }

    componentWillReceiveProps(nextProps) {
        this.state = {
            operatories: nextProps.operatories
        }
    }

    changeStatus(index, status) {
        // if(this.state.operatories[index].currentStatus === 'empty' && status !== 'empty') {
        //     const emptyStyle = 'linear-gradient(90deg, transparent 50%, #f8f8f8 50%), linear-gradient(90deg, #f8f8f8 50%, transparent 50%)';
        //     this.state.operatories[index].backgroundImage = emptyStyle;
        // }
        this.state.operatories[index].nextStatus = status;
        this.setState(this.state);
    }

    updateProcedure(e, operatoryId) {
        console.log('etargetval', e.target.value);
        const index = _.findIndex(this.state.operatories, op => {return op.id === operatoryId});
        const operatory = this.state.operatories[index];

        this.state.operatories[index].currentProcedureCode = e.target.value;
        const currentCode = _.find(this.state.procedureCodes, pc => {return pc.key === e.target.value});
        this.state.operatories[index].currentProcedureDescription = currentCode.value;
        this.setState(this.state);
    }

    sendMessage(operatoryId) {
        const index = _.findIndex(this.state.operatories, op => {return op.id === operatoryId});
        const operatory = this.state.operatories[index];
        if (operatory.nextStatus && operatory.nextStatus !== operatory.currentStatus) {
            operatory.currentStatus = operatory.nextStatus;
            operatory.lastUpdated = moment().unix();
        }
        delete operatory.nextStatus;
        this.state.operatories[index] = operatory;
        this.setState(this.state);
        const event = {
            operatoryId: operatory.id,
            officeId: operatory.officeId,
            practiceId: this.props.practice.id,
            status: operatory.currentStatus,
            procedureCode: operatory.currentProcedureCode,
            procedureMessage: operatory.currentProcedureDescription,
            createdAt: moment().unix()
        };
        firebaseDatabaseService.updateOperatory(this.state.operatories[index], () => {
            console.log('updated Operatory Procedure')
        });
        firebaseDatabaseService.sendMessage(event, event => {
           console.log('Sent event!');
        });
    }

    didClickAdd() {
        //TODO: Replace officeId with the actual office ID rather than this dummy value.
        const op = {
          currentStatus: 'empty',
            lastUpdated: moment().unix(),
            createdAt: moment().unix(),
            // name: 'Name this OP',
            officeId: this.state.office.id,
            currentProcedureCode: '',
            currentProcedureDescription: ''
        };
        firebaseDatabaseService.createOperatory(op, () => {
            console.log('successfully created OP', op);
        });
    }

    didClickRemove(operatoryId) {
        const op = _.find(this.state.operatories, operatory => {return operatory.id === operatoryId});
        const r = window.confirm('Are you sure you would like to remove this OP?');
        if (r === true) {
            console.log('pressed ok', op);
            firebaseDatabaseService.deleteOperatory(op, () => {
                console.log('deleted OP', op);
            });
        }  else {
            console.log('pressed cancel');
        }
    }

    componentDidMount() {
        firebaseDatabaseService.getOffice(this.props.currentUser.id, office => {
            console.log('got the office!', office);
            this.state.office = office;
            this.setState(this.state);
            firebaseDatabaseService.getOperatoriesByOffice(office.id, ops => {
                console.log('fetched OPS');
                this.setState({operatories: ops});
            }, ref => {
                this.state.refs.push(ref);
                this.setState(this.state);
            });
        }, ref => {
            this.state.refs.push(ref);
            this.setState(this.state);
        });

        firebaseDatabaseService.fetchProcedureCodes(codes => {
            this.setState({procedureCodes: codes});
        }, ref => {
            this.state.refs.push(ref);
            this.setState(this.state);
        });
    }

    componentWillUnmount() {
        for (let ref of this.state.refs) {
            ref.off();
        }
    }

    render() {
        return (
            <div className="container rooms">
                <div className="row">
                    <button className="btn room-action" onClick={this.didClickAdd.bind(this)}>
                        <Icon name="plus" style={{marginTop: '4px'}}/>
                        &nbsp;&nbsp;Add OP
                    </button>
                </div>
                <div className="rooms-row">
                    {
                        this.state.operatories.map((operatory, index) => {
                            return (
                              <Operatory
                                  operatory={operatory}
                                  procedureCodes={this.state.procedureCodes}
                                  didClickRemove={() => {this.didClickRemove(operatory.id)}}
                                  changeStatus={status => {this.changeStatus(index, status)}}
                                  updateProcedure={e => {this.updateProcedure(e, operatory.id)}}
                                  sendMessage={() => {this.sendMessage(operatory.id)}}
                                  key={index}
                              />
                            );
                        })
                    }
                </div>
                <Footer />
            </div>
        );
    }
}

export default Operatories;