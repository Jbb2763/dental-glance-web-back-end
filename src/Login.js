import React, { Component } from 'react';
import {firebaseAuthService} from './firebase.auth.fact'
import { Route, Router, Redirect, Link } from 'react-router-dom'

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            errors: [],
            isLoggedIn: false
        }
    }

    emailChanged(e) {
        this.state.email = e.target.value;
        this.setState(this.state);
    }

    passwordChanged(e) {
        this.state.password = e.target.value;
        this.setState(this.state);
    }

    submit() {
        this.state.errors = [];
        this.setState(this.state);
        firebaseAuthService.loginUser(this.state.email, this.state.password)
            .then(user => {
                console.log('controller successfully logged in user');
                this.state.isLoggedIn = true;
                this.setState(this.state);
            }, error => {
                this.state.errors.push(error.message);
                this.setState(this.state);
            });
    }

    componentDidMount() {
        firebaseAuthService.currentlyLoggedInUser(user => {
            this.state.isLoggedIn = !!user;
            this.setState(this.state);
        });
    }

    render() {
        return (
            this.state.isLoggedIn ?
                <Redirect to="/"/>
            :
                <div className="container auth">
                    <h1 className="centered-text">LOGIN</h1>
                    <div className="form-set">
                        <label htmlFor="email"></label>
                        <input type="text" id="email" placeholder="Enter your email" value={this.state.email} onChange={this.emailChanged.bind(this)}/>
                        <br/>
                        <label htmlFor="password"></label>
                        <input type="password" id="password" placeholder="Password (min 6 characters)" value={this.state.password} onChange={this.passwordChanged.bind(this)}/>
                        <br/>
                        <p className="errors">{this.state.errors}</p>
                        <button onClick={this.submit.bind(this)}>Submit</button>
                        <br/>
                        <Link to="/sign_up">Sign Up Instead</Link>
                    </div>
                </div>
        );
    }
}

export default Login;