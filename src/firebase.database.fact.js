//Firebase Database Factory
import moment from 'moment';
import _ from 'lodash';

const database = window.firebase.database();
//TODO: Implement promises here
export let firebaseDatabaseService = {
    //Practices
    createPractice: (practice) => {
      return new Promise((resolve, reject) => {
          const newRef = database.ref('Practices').push();
          newRef.set(practice)
              .then(() => {
                  practice.id = newRef.key;
                  resolve(practice);
              }, error => {
                  reject(error);
              });
      });
    },

    getPractice: (uid, callback, refHandle) => {
      const ref = database.ref('Permissions');
      refHandle(ref);
      return ref.child(`practice_${uid}`).on('value', snapshot => {
          const practice = snapshot.val();
          if (practice && !_.isEmpty(practice)) {
            practice.id = snapshot.key;
          }
          callback(practice);
      });
    },

    //Offices
    //TODO: This only gets one office right now. Adjust it to multiple offices in the future
    getOffice: (uid, callback, refHandle) => {
        const ref = database.ref('Permissions');
        refHandle(ref);
        ref.orderByChild('userId').equalTo(uid).once('value', snapshot => {
            console.log('snapshot office permission', snapshot.val());
            let permissions = [];
            snapshot.forEach(childSnapshot => {
                const permission = childSnapshot.val();
                permission.id = childSnapshot.key;
                if (permission.officeId) {
                    permissions.push(permission);
                }
            });
            if (permissions.length > 0) {
                database.ref('Offices').child(permissions[0].officeId).once('value', snapshot => {
                    let office = snapshot.val();
                    office.id = snapshot.key;
                    callback(office);
                });
            } else {
                callback({});
            }
        });
    },

    //Ops
    getOperatoriesByOffice: function(officeId, success, refHandle) {
        const ref = database.ref('Operatories');
        refHandle(ref);
        ref.child(officeId).on('value', snapshot => {
            let operatories = [];
            snapshot.forEach(function(childSnapshot) {
                const operatory = childSnapshot.val();
                operatory.id = childSnapshot.key;
                operatories.push(operatory);
            });
            success(operatories);
        })
    },

    createOperatory: (op, success) => {
        var opId = 'op1';
        database.ref('Operatories').child(op.officeId).once('value', snapshot => {
            if (snapshot.hasChild(opId)) {
                console.log(`has child ${opId}`);
                while (snapshot.hasChild(opId)) {
                    opId = 'op' + (parseInt(opId.replace('op', '')) + 1);
                    console.log('new opId', opId);
                }
            } else {
                console.log('does not have child');
            }
            op.id = opId;
            database.ref('Operatories').child(op.officeId).child(opId).set(op).then(() => {
                success(op);
            });
        });
    },

    updateOperatory: (op, success) => {
        database.ref('Operatories').child(op.officeId).child(op.id).update(op).then(() => {
            success();
        }).catch(error => {
            console.log(error.message);
        })
    },

    deleteOperatory: (op, success) => {
        database.ref('Operatories').child(op.officeId).child(op.id).set(null)
            .then(() => {
                success();
            });
    },

    //Events
    sendMessage: function(event, success) {
        database.ref('Events').push(event).then(event => {
            success();
        }).catch(error => {
            console.log(error.message);
        });
    },

    //Registrations
    createRegistration: (userId, practiceId) => {
        return database.ref('Registrations').child(practiceId).set({userId: userId});
    },

    //Users
    createUser: (uid, user) => {
        user['createdAt'] = moment().unix();
        return database.ref('Users').child(uid).set(user);
    },

    getUser: (userId, successCallback) => {
        console.log('uid found', userId);
        return database.ref('Users').child(userId).once('value', snapshot => {
            console.log('found user:', snapshot.val());
            let u = snapshot.val();
            u.uid = snapshot.key;
            successCallback(u);
        });
    },

    //Status and Procedure codes
    fetchProcedureCodes: (successCallback, refHandle) => {
        const ref = database.ref('ProcedureCodes');
        refHandle(ref);
        ref.on('value', snapshot => {
            let codes = [];
            snapshot.forEach(childSnapshot => {
                const code = {};
                code.key = childSnapshot.key;
                code.value = childSnapshot.val();
                console.log('code', code);
                codes.push(code);
            });
            successCallback(codes);
        });
    }
};