import React, { Component } from 'react';
import Header from './Header';
import Operatories from './Operatories';
import Login from './Login';
import Registration from './Registration';
import { Route, Redirect } from 'react-router-dom'
import {firebaseAuthService} from './firebase.auth.fact'
import {firebaseDatabaseService} from './firebase.database.fact'

const PropsRoute = ({ component, ...rest }) => {
    return (
        <Route {...rest} render={routeProps => {
            return renderMergedProps(component, routeProps, rest);
        }}/>
    );
};

const renderMergedProps = (component, ...rest) => {
    const finalProps = Object.assign({}, ...rest);
    return (
        React.createElement(component, finalProps)
    );
};

class Main extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoggedIn: false,
            currentUser: {},
            refs: []
        }
    }

    logout() {
        console.log('logged out');
        firebaseAuthService.logoutUser()
            .then(() => {
                console.log('about to call callback');
                this.state.isLoggedIn = false;
                this.state.currentUser = {};
                this.setState(this.state);
            });
    }

    componentDidMount() {
        firebaseAuthService.currentlyLoggedInUser(user => {
            this.state.isLoggedIn = !!user;
            this.state.currentUser = user;
            firebaseDatabaseService.getPractice(user.id, practice => {
                this.state.practice = practice;
                this.setState(this.state);
            }, ref => {
                this.state.refs.push(ref);
                this.setState(this.state);
            });
        });
    }

    componentWillUnmount() {
        for (let ref of this.state.refs) {
            ref.off();
        }
    }

    render() {
        let body = document.body;
        if (this.props.location.pathname === '/') {
            body.style.backgroundColor = '#203439'
        } else {
            body.style.backgroundColor = 'white';
        }
        return (
            this.state.isLoggedIn || this.props.location.pathname === '/login' || this.props.location.pathname === '/sign_up'  ?
                <div>
                    <Header isLoggedIn={this.state.isLoggedIn} currentUser={this.state.currentUser} logout={this.logout.bind(this)}/>
                    <div className="container">
                        <PropsRoute exact path="/" component={Operatories} currentUser={this.state.currentUser} practice={this.state.practice} />
                        <Route path="/login" component={Login}/>
                        <Route path="/sign_up" component={Registration}/>
                    </div>
                </div>
            :
                <Redirect to="/login"/>

        );
    }
}

export default Main;