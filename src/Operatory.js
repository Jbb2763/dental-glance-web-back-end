import React, { Component } from 'react';
import moment from 'moment';

class Operatory extends Component {
    constructor(props) {
        super(props);
        this.state = {
            backgroundImage: ''
        };
        this.interval = {};
    }

    // shouldComponentUpdate(nextProps, nextState) {
    //     const c1 = (this.props.operatory.currentStatus !== nextProps.operatory.currentStatus);
    //     const c2 = (this.state.backgroundImage !== nextState.backgroundImage);
    //     // const c2 = (this.props.operatory.backgroundImage !== nextProps.operatory.backgroundImage);
    //     // console.log('p', this.props.operatory.backgroundImage);
    //     // console.log('next p', nextProps.operatory.backgroundImage);
    //     return c1 || c2;
    // }

    componentDidMount() {
        this.interval = setInterval(() => {
            if (this.props.operatory.currentStatus !== 'empty') {
                const currentTime = moment();
                const lastUpdated = moment.unix(this.props.operatory.lastUpdated);
                const elapsedTime = moment.duration(currentTime.diff(lastUpdated)).asSeconds();
                this.state.backgroundImage = this.setCircleStyleByPercent((elapsedTime/3600)*100, this.props.operatory.currentStatus);
            } else if (this.props.operatory.time !== 0) {
                this.state.backgroundImage = this.setCircleStyleByPercent(100, this.props.operatory.currentStatus);
            }
            this.setState(this.state);
        }, 2000);
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    setCircleStyleByPercent(percent, status) {
        let color = '#39D661'; //green
        let degrees, style;
        if(status === 'pending'){
            color = '#FFB100';
        } else if(status === 'empty') {
            color = '#ABABAB';
        }

        if(percent <= 50){
            degrees = 90 + ( 360 * (percent/100) );
            style = 'linear-gradient('+degrees+'deg, transparent 50%, #f8f8f8 50%), linear-gradient(90deg, #f8f8f8 50%, transparent 50%)';
        } else {
            degrees = 90 + ( 180 * ((percent-50)/50) );
            // degrees = 270;
            style = 'linear-gradient('+degrees+'deg, transparent 50%, '+color+' 50%), linear-gradient(90deg, #f8f8f8 50%, transparent 50%)';
        }
        return style;
    }

    humanizedDuration(lastUpdated) {
        const duration = moment.duration(moment().diff(moment.unix(lastUpdated)));
        return parseInt(duration.asMinutes()) + 'm ' + Math.round(duration.asSeconds() % 60) + 's';
    }

    render() {
        return (
            <div className="room">
                <div className="room-header">
                    { this.props.operatory.id }
                    <i className="icon icon-dots" onClick={this.props.didClickRemove}></i>
                </div>
                <div className="room-info">
                    <div className={'circle-container ' + this.props.operatory.currentStatus}>
                        <div id={'circle' + this.props.operatory.id} className="dynamic-circle" style={{'backgroundImage': this.state.backgroundImage || 'initial' }}>
                            <div className="white-circle">
                                <div className="circle green">
                                    {this.props.operatory.currentStatus === 'empty' ? 'OP EMPTY' : this.props.operatory.currentStatus === 'pending' ? 'PATIENT IN OP' : 'PATIENT READY'}
                                    <span>{this.humanizedDuration(this.props.operatory.lastUpdated)}</span>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div className="buttons-container">
                        <div className="tip">Set op’s status:</div>
                        <div className="btn btn-gray" onClick={() => {this.props.changeStatus('empty')}} disabled={(this.props.operatory.nextStatus && this.props.operatory.nextStatus !== 'empty') || (this.props.operatory.currentStatus !== 'empty' && !this.props.operatory.nextStatus)}>
                            <i className="icon icon-tick"></i>
                            <span className="btn-title">OP EMPTY</span>
                        </div>
                        <div className="btn btn-orange" onClick={() => {this.props.changeStatus('pending')}} disabled={!(this.props.operatory.nextStatus === 'pending' || this.props.operatory.currentStatus === 'pending' && !this.props.operatory.nextStatus)}>
                            <i className="icon icon-tick"></i>
                            <span className="btn-title">PATIENT IN OP</span>
                        </div>
                        <div className="btn btn-green" onClick={() => {this.props.changeStatus('ready')}} disabled={!(this.props.operatory.nextStatus === 'ready' || this.props.operatory.currentStatus === 'ready' && !this.props.operatory.nextStatus)}>
                            <i className="icon icon-tick"></i>
                            <span className="btn-title">PATIENT READY</span>
                        </div>
                    </div>
                </div>
                <div className='dash-line'></div>
                <div className="room-footer">
                    <select value={this.props.operatory.currentProcedureCode} onChange={this.props.updateProcedure}>
                        {
                            this.props.procedureCodes.map((code, i) => {
                                return (
                                    <option value={code.key} key={i}>{code.value}</option>
                                );
                            })
                        }
                    </select>
                    <div className="btn btn-default" onClick={this.props.sendMessage}>SEND</div>
                </div>
            </div>
        );
    }
}

export default Operatory;