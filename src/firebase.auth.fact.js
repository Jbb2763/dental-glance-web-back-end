//Firebase Auth Factory
import {firebaseDatabaseService} from './firebase.database.fact'
import moment from 'moment'

export let firebaseAuthService = {
    loginUser: (email, password) => {
        // firebase.auth().signInWithEmailAndPassword('kristoph@test.com', '12345678').then((user) => {
        // returns promise
        return window.firebase.auth().signInWithEmailAndPassword(email, password);
    },

    registerUser: (firstName, lastName, email, password, practiceName, successCallback, errorCallback) => {
        return window.firebase.auth().createUserWithEmailAndPassword(email, password)
            .then(user => {
                // Perform Registration and call callback
                firebaseDatabaseService.createUser(user.uid, {firstName: firstName, lastName: lastName, email: email})
                    .then(() => {
                        //Create practice
                        const practice = {
                            name: practiceName,
                            createdBy: user.uid,
                            createdAt: moment().unix()
                        };
                        firebaseDatabaseService.createPractice(practice)
                            .then(practice => {
                                console.log('successfully created practice', practice);
                                firebaseDatabaseService.createRegistration(user.uid, practice.id)
                                    .then(registration => {
                                        console.log('reg successful at Registrations node', registration);
                                        successCallback(user);
                                    }, error => {
                                        console.log('error saving registration to Registration node', error);
                                    });
                            }, error => {
                                console.log('error creating practice', error);
                            });
                    }, err => {
                        console.log('error creating user in Users node', err.message);
                    });
                console.log('successfully created user. Now sending registration to Registration node', user);
            }, error => {
                console.log('error with user registration', error);
                errorCallback(error);
            });
    },

    logoutUser: () => {
        return window.firebase.auth().signOut();
    },

    currentlyLoggedInUser: (callback) => {
        return window.firebase.auth().onAuthStateChanged(user => {
            if (user) {
                firebaseDatabaseService.getUser(user.uid, u => {
                    u.id = user.uid;
                    callback(u);
                });
            }
        });
    }
};