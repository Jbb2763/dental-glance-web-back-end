import React, { Component } from 'react';
import {firebaseAuthService} from './firebase.auth.fact'
import { Switch, Route, Router, Redirect } from 'react-router-dom'

class Header extends Component {
    render() {
        const logoutButton = this.props.isLoggedIn ? (<li onClick={this.props.logout}>Logout</li>) : '';
        return (
                <header>
                    <div className="container">
                        <div className="logo">DENTAL GLANCE</div>
                        <ul className="menu">
                            <li><div className="marcus btn btn-outline">ABOUT</div></li>
                            <li><div className="btn btn-outline">FAQs</div></li>
                            <li><div className="btn btn-outline">CONTACT</div></li>
                            {/*<li>John Appleseed <i className="icon icon-dropdown"></i></li>*/}
                            <li>{this.props.currentUser.firstName} {this.props.currentUser.lastName}</li>
                            {logoutButton}
                        </ul>
                        <div className="clearfix"></div>
                    </div>
                </header>
        );
    }
}

export default Header;