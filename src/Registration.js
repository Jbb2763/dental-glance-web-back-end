import React, { Component } from 'react';
import {firebaseAuthService} from './firebase.auth.fact'
import {firebaseDatabaseService} from './firebase.database.fact'
import { Switch, Route, Router, Redirect, Link } from 'react-router-dom'

class Registration extends Component {
    constructor(props) {
        super(props);
        console.log('in registration');
        this.state = {
            email: '',
            password: '',
            firstName: '',
            lastName: '',
            practiceName: '',
            errors: [],
            isLoggedIn: false
        }
    }

    fNameChanged(e) {
        this.state.firstName = e.target.value;
        this.setState(this.state);
    }

    lNameChanged(e) {
        this.state.lastName = e.target.value;
        this.setState(this.state);
    }

    emailChanged(e) {
        this.state.email = e.target.value;
        this.setState(this.state);
    }

    passwordChanged(e) {
        this.state.password = e.target.value;
        this.setState(this.state);
    }

    practiceNameChanged(e) {
        this.state.practiceName = e.target.value;
        this.setState(this.state);
    }

    submit() {
        this.state.errors = [];
        this.setState(this.state);

        firebaseAuthService.registerUser(this.state.firstName, this.state.lastName, this.state.email, this.state.password, this.state.practiceName, user => {
            if (user) {
                this.state.isLoggedIn = true;
                this.setState(this.state);
            }
        }, error => {
            this.state.errors.push(error.message);
            this.setState(this.state);
        });
    }

    componentDidMount() {
        firebaseAuthService.currentlyLoggedInUser(user => {
            console.log('cdm user', user);
            this.state.isLoggedIn = !!user;
            this.setState(this.state);
        });
    }

    render() {
        return (
            this.state.isLoggedIn ?
                <Redirect to="/"/>
                :
                <div className="container auth">
                    <h1 className="centered-text">REGISTER</h1>
                    <div className="form-set">
                        <label htmlFor="first-name"></label>
                        <input type="text" id="first-name" placeholder="Enter your first name" value={this.state.firstName} onChange={this.fNameChanged.bind(this)}/>
                        <br/>
                        <label htmlFor="last-name"></label>
                        <input type="text" id="last-name" placeholder="Enter your last name" value={this.state.lastName} onChange={this.lNameChanged.bind(this)}/>
                        <br/>
                        <label htmlFor="email"></label>
                        <input type="text" id="email" placeholder="Enter your email" value={this.state.email} onChange={this.emailChanged.bind(this)}/>
                        <br/>
                        <label htmlFor="password"></label>
                        <input type="password" id="password" placeholder="Password (min 6 characters)" value={this.state.password} onChange={this.passwordChanged.bind(this)}/>
                        <br/>
                        <label htmlFor="practice-name"></label>
                        <input type="text" id="password" placeholder="Enter a Unique Practice Name" value={this.state.practiceName} onChange={this.practiceNameChanged.bind(this)}/>
                        <br/>
                        <p className="errors">{this.state.errors}</p>
                        <button onClick={this.submit.bind(this)}>Submit</button>
                        <br/>
                        <Link to="/login">Login Instead</Link>
                    </div>
                </div>
        );
    }
}

export default Registration;